%% Init
% Clear workspace and close figures
clear, close all
clc

addpath('./lib')

% Set significance levels
alpha = [0.05, 0.01];

%% I/O files
% I/O files and directories definition
filename = 'Datiolio.xls';
outputdir = 'Results';

logFile = 'Log.xls';
resultFile = 'Results.xls';

%% Measurement campaign size identification
% Number of levels
[~, labs] = xlsfinfo(filename);
Nlab = numel(labs);

% Number of laboratories involved
[~, levels] = xlsread(filename,1);
Nlvl = numel(levels);

%% Test loop begins here
if ~exist(outputdir, 'dir')
    mkdir(outputdir)
end

hMandelResults = zeros(Nlab, Nlvl);
kMandelResults = zeros(Nlab, Nlvl);
for iLvl = 1:Nlvl
    %% Finding labs with valid results for current level
    currentLevel = levels{iLvl};
    validLabs = {};
    values = {};
    for i = 1:Nlab
        [valTmp, levTmp] = xlsread(filename, i);
        iLiv = find(strcmp(levTmp, currentLevel));
        
        if ~isempty(iLiv)
            validLabs = [validLabs, labs{i}]; %#ok<AGROW> (unpredictable var size)
            values = [values; valTmp(:,iLiv)]; %#ok<AGROW> (unpredictable var size)
        end
    end
    
    % Number of valid measurements per laboratory
    lengthLab = cellfun(@numel, values);
    
    % Spreadsheet and console output for valid labs
    fprintf('Laboratories with valid results for %s are\n\n', currentLevel)
    disp(validLabs)
    
    xlswrite([outputdir, '\', logFile], { ...
        sprintf('%s', currentLevel); ...
        '';
        'Laboratories with valid results for current level:'}, ...
        sprintf('Sheet%d', iLvl), 'A1')
    
    xlswrite([outputdir, '\', logFile], validLabs, sprintf('Sheet%d', iLvl), 'A5')

    %% Dispersion and boxplot
    % All measurements for the current level
    allValues = cell2mat(values); allValues(isnan(allValues)) = [];

    % Dispersion plot
    hDispersion = SCdispersionplot(values, Nlab, currentLevel, validLabs);

    % Grafico scatola e baffi
    hBoxplot = SCboxplot(values, currentLevel, validLabs);

    %% Laboratory (cell) averages, stardard deviations and variances
    m = cellfun(@mean, values);
    s = cellfun(@std, values);
    v = cellfun(@var, values);
    
    % Spreadsheet and console output for cell averages
    fprintf('Cell averages - %s\n', currentLevel)
    disp(m)
    
    xlswrite([outputdir, '\', resultFile], ...
        currentLevel, ...
        sprintf('Sheet%d', iLvl), 'A1')
    
    xlswrite([outputdir, '\', resultFile], ...
        'Cell averages', ...
        sprintf('Sheet%d', iLvl), 'A3')
    
    xlswrite([outputdir, '\', resultFile], m, ...
        sprintf('Sheet%d', iLvl), 'A4')

    % Spreadsheet and console output for cell standard deviations
    fprintf('Cell standard deviations - %s\n', currentLevel)
    disp(s)
    
    xlswrite([outputdir, '\', resultFile], ...
        'Cell standard deviations', ...
        sprintf('Sheet%d', iLvl), 'C3')
    
    xlswrite([outputdir, '\', resultFile], ...
        s, ...
        sprintf('Sheet%d', iLvl), 'C4')

    %% Absolute measurement range per laboratory
    d = zeros(size(values));
    for i = 1:numel(values)
        if length(values{i}) > 1
            d(i) = range(values{i});
        else
            d(i) = values{i};
        end
    end
    
    % Spreadsheet and console output for cell ranges
    fprintf('Cell ranges - %s\n', currentLevel)
    disp(d)
    
    xlswrite([outputdir, '\', resultFile], ...
        'Cell ranges', ...
        sprintf('Sheet%d', iLvl), ...
        sprintf('C%d', (Nlab+5)))
    
    xlswrite([outputdir, '\', resultFile], ...
        d, ...
        sprintf('Sheet%d', iLvl), ...
        sprintf('C%d', (Nlab+6)))
        
    %% Grand mean and standard deviation
    mm = mean(allValues);
    ss = std(allValues);
    
    % Spreadsheet and console output for grand mean and standard deviation
    fprintf('Grand mean - %s\n', currentLevel)
    disp(mm)
    
    xlswrite([outputdir, '\', resultFile], {'Grand mean'; mm}, ...
        sprintf('Sheet%d', iLvl), 'E3')

    fprintf('Overall standard deviation - %s\n', currentLevel)
    disp(ss)
    
    xlswrite([outputdir, '\', resultFile], {'Overall standard deviation'; ss}, ...
        sprintf('Sheet%d',iLvl), 'E6')

    %% Mandel's h/k statistics (inter/intralaboratory coherence)
    [hMandelResults(:, iLvl), H95, H99] = ...
        SChMandel(alpha, m, mm, Nlab, validLabs, iLvl, levels, [outputdir, '\', logFile], outputdir);
    
    [kMandelResults(:, iLvl), K95, K99] = ...
        SCkMandel(alpha, s, lengthLab, Nlab, validLabs, iLvl, levels, [outputdir, '\', logFile], outputdir);

    %% Cochran and Grubbs tests
    SCcochran(alpha, v, lengthLab, Nlab, iLvl, levels, [outputdir, '\', resultFile], [outputdir, '\', logFile]);

    SCgrubbs(alpha, m, Nlab, iLvl, levels, [outputdir, '\', resultFile], [outputdir, '\', logFile]);

    %% Scarti tipo di ripetibilitÓ e riproducibilitÓ
    SCreprip(m, mm, s, lengthLab, Nlab, iLvl, levels, [outputdir, '\', resultFile]);

    %% ANOVA
    anovatable = SCanova(values, lengthLab, Nlab, iLvl, levels, validLabs, [outputdir, '\', resultFile]);

end

%% Mandel h/k bar graphs
% FIXME save these figures on files
figure
bar(hMandelResults)
set(gca, 'XTickLabel', labs)
line(get(gca, 'XLim'), [H95, H95], 'linestyle', '--', 'color', 'red')
line(get(gca, 'XLim'), [H99, H99], 'linestyle', '--', 'color', 'red')
line(get(gca, 'XLim'), [-H95, -H95], 'linestyle', '--', 'color', 'red')
line(get(gca, 'XLim'), [-H99, -H99], 'linestyle', '--', 'color', 'red')

figure
bar(kMandelResults)
set(gca, 'XTickLabel', labs)
line(get(gca, 'XLim'), [K95, K95], 'linestyle', '--', 'color', 'red')
line(get(gca, 'XLim'), [K99, K99], 'linestyle', '--', 'color', 'red')

rmpath('./lib')

