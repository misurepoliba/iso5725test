function [k, K95out, K99out, hPlotk] = SCkMandel(alpha, s, lengthlvl, Nlab, labs, iLvl, levels, logFile, varargin)
% KMANDEL: Calcola e plotta la statistica k di Mandel (coerenza intralaboratorio)
%   FIXME adjust docs
%   kmandel(s,lengthlvl,numlab,laboratori,livello,idlvl)
%
%   s : Scarti tipo dei singoli laboratori (dispersione nella cella)
%   lengthlvl : Vettore contenente il numero di risultati di prova per
%               ciasun laboratorio
%   numlab : Numero di laboratori partecipanti
%   laboratori : Identificativo dei laboratori partecipanti
%   livello : Livello corrente della prova
%   idlvl : Identificativo del livelli
%   idlab : Identificativo dei laboratori

% Check significance levels
alpha1 = alpha(1); alpha2 = alpha(2);
if alpha1 <= alpha2
    error('Incorrect significance levels')
end

K95n2=[1.65; 1.76; 1.81; 1.85; 1.87; 1.88; 1.90; 1.90; 1.91; 1.92; 1.92; 1.92; 1.93; 1.93; 1.93; 1.93; 1.93; 1.94];
K95n3=[1.53; 1.59; 1.62; 1.64; 1.66; 1.67; 1.68; 1.68; 1.69; 1.69; 1.69; 1.70; 1.70; 1.70; 1.70; 1.71; 1.71; 1.71];
K95n4=[1.45; 1.50; 1.53; 1.54; 1.55; 1.56; 1.57; 1.57; 1.58; 1.58; 1.58; 1.59; 1.59; 1.59; 1.59; 1.59; 1.59; 1.59];
K95n5=[1.40; 1.44; 1.46; 1.48; 1.49; 1.50; 1.50; 1.50; 1.51; 1.51; 1.51; 1.52; 1.52; 1.52; 1.52; 1.52; 1.52; 1.52];
K95n6=[1.37; 1.40; 1.42; 1.43; 1.44; 1.45; 1.45; 1.46; 1.46; 1.46; 1.46; 1.47; 1.47; 1.47; 1.47; 1.47; 1.47; 1.47];
K95n7=[1.34; 1.37; 1.39; 1.40; 1.41; 1.41; 1.42; 1.42; 1.42; 1.42; 1.43; 1.43; 1.43; 1.43; 1.43; 1.43; 1.43; 1.43];
K95n8=[1.32; 1.35; 1.36; 1.37; 1.38; 1.38; 1.39; 1.39; 1.39; 1.40; 1.40; 1.40; 1.40; 1.40; 1.40; 1.40; 1.40; 1.40];

K99n2=[1.71; 1.91; 2.05; 2.14; 2.20; 2.25; 2.29; 2.32; 2.34; 2.36; 2.38; 2.39; 2.41; 2.42; 2.44; 2.44; 2.44; 2.45];
K99n3=[1.64; 1.77; 1.85; 1.90; 1.94; 1.97; 1.99; 2.00; 2.01; 2.02; 2.03; 2.04; 2.05; 2.05; 2.06; 2.06; 2.07; 2.07];
K99n4=[1.58; 1.67; 1.73; 1.77; 1.79; 1.81; 1.82; 1.84; 1.85; 1.85; 1.86; 1.87; 1.87; 1.88; 1.88; 1.88; 1.89; 1.89];
K99n5=[1.53; 1.60; 1.65; 1.68; 1.70; 1.71; 1.73; 1.74; 1.74; 1.75; 1.76; 1.76; 1.76; 1.77; 1.77; 1.77; 1.78; 1.78];
K99n6=[1.49; 1.55; 1.59; 1.62; 1.63; 1.65; 1.66; 1.66; 1.67; 1.68; 1.68; 1.69; 1.69; 1.69; 1.69; 1.70; 1.70; 1.70];
K99n7=[1.46; 1.51; 1.55; 1.57; 1.58; 1.59; 1.60; 1.61; 1.62; 1.62; 1.63; 1.63; 1.63; 1.63; 1.64; 1.64; 1.64; 1.64];
K99n8=[1.43; 1.48; 1.51; 1.53; 1.54; 1.55; 1.56; 1.57; 1.57; 1.58; 1.58; 1.58; 1.59; 1.59; 1.59; 1.59; 1.59; 1.60];
% Matrici contenenti i valori critici della statistica k di Mandel
% Ad ogni riga corrisponde un numero di laboratori partecipanti (valori
% per numlab 3:20) FIXME translate doc
K95=[K95n2 K95n3 K95n4 K95n5 K95n6 K95n7 K95n8];
K99=[K99n2 K99n3 K99n4 K99n5 K99n6 K99n7 K99n8];

% Test critical values
kcv = @(p, n, alpha) sqrt(p./(1+ (p-1).*finv(alpha, (p-1)*(n-1),n-1)));

% k computation
a=0;
for j=1:Nlab
    a=a+s(j)^2;
end

k = zeros(Nlab,1);
for i=1:Nlab
    k(i)=s(i)*sqrt(Nlab/a);
end

K95out = K95(Nlab-2);
K99out = K99(Nlab-2);

% Sorted cell results
lengthlvlord=sort(lengthlvl);
numres(1)=lengthlvlord(1);
j=1;
for i=2:Nlab
    if lengthlvlord(i)~=lengthlvlord(i-1)
        j=j+1;
        numres(j)=lengthlvlord(i);
    end
end

% Valori per numcol 1:5
numcol=length(numres);
if numcol==1
    colorSeq={'b'};
else
    if numcol==2
        colorSeq={'b','r'};
    else
        if numcol==3
            colorSeq={'b','r','g'};
        else
            if numcol==4
                colorSeq={'b','r','g','y'};
            else
                if numcol==5
                    colorSeq={'b','r','g','y','m'};
                end
            end
        end
    end
end

% Color order FIXME deprecate?
for i=1:Nlab
    for j=1:numcol
        if lengthlvl(i)==numres(j)
            colorord{i} = colorSeq{j};
        end
    end
end

% Mandel k plot
hPlotk = figure;
hAx = plot(1, k(1), '*');
title(sprintf('Mandel''s k statistic - %s', levels{iLvl}))
xlabel('Laboratory')
ylabel('k')
xlim([1, Nlab])
set(hAx,'markeredgecolor',colorord{1});
set(gca,'xtick',1:Nlab)
set(gca,'xticklabel',labs)
hold on
for i = 2:Nlab
    hAx = plot(i,k(i),'*');
    set(hAx,'markeredgecolor',colorord{i});
end
hold off
hold on
for i=1:numcol
    if numres(i)~=1 % No critical value for just a single result
        hAx = plot(kcv(Nlab, numres(i), alpha1) *ones(1,Nlab),colorSeq{i});
        set(hAx,'linestyle','--', 'color', 'blue');
        hAx = plot(kcv(Nlab, numres(i), alpha2) * ones(1,Nlab),colorSeq{i});
        set(hAx,'linestyle','--', 'color', 'red');
    end
end
hold off
grid on
% FIXME convert plot into line commands

% Performing test and logging results
fprintf('Mandel''s k statistic - %s\n\n', levels{iLvl})
log = {'Mandel''s k statistic'};
for i = 1:Nlab
    if lengthlvl(i)~=1 % No critical value for just a single result
        for j=1:numcol
        
            % Computing critical values
            K_crit_alpha1 = kcv(Nlab, numres(j), alpha1);
            K_crit_alpha2 = kcv(Nlab, numres(j), alpha2);

            if colorord{i}==colorSeq{j}
                if k(i) <= K_crit_alpha1
                    fprintf('k for laboratory %s is correct\n\n',labs{i})
                    log=[log;{sprintf('k for laboratory %s is correct',labs{i})}];
                else
                    if k(i) <= K_crit_alpha2
                        fprintf('k for laboratory %s shows dispersed values\n\n',labs{i})
                        log=[log;{sprintf('k for laboratory %s shows dispersed values',labs{i})}];
                    else
                        fprintf('k for laboratory %s shows anomalous values\n\n',labs{i})
                        log=[log;{sprintf('k for laboratory %s shows anomalous values',labs{i})}];
                    end
                end
            end
        end
    else
        fprintf('k for laboratory %s has not been computed (too few data)\n\n',labs{i})
        log=[log;sprintf('k for laboratory %s has not been computed (too few data)',labs{i})];
    end
end
xlswrite(logFile, log, sprintf('Sheet%d', iLvl), sprintf('A%d', (Nlab+9)))

% Export on file
if ~isempty(varargin)
    cd(varargin{1})
    print('-dpng', sprintf('Mandel k - %s.png', levels{iLvl}))
    cd ..
end

end