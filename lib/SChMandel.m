function [h, H95out, H99out, hPloth] = SChMandel(alpha, m, grandMean, Nlab, labs, iLvl, levels, logFile, varargin)
% HMANDEL computes and plots Mandel's h statistic (interlaboratory coherence)
%   HMANDEL(m, media, numlab, laboratori, livello, idlvl)
%
%   m:          laboratory (cell) means
%   grandMean:  measurements grand mean
%   Nlab:       number of laboratories involved
%   labs:       lab names (cell array)
%   iLvl:       level index
%   levels:     level names (cell array)

% Check significance levels
alpha1 = alpha(1); alpha2 = alpha(2);
if alpha1 <= alpha2
    error('Incorrect significance levels')
end

hcv = @(p,alpha) (p-1).*tinv(1-alpha/2, p-2) ./ sqrt(p.*(tinv(1-alpha/2, p-2).^2 + p - 2));
Hcrit95 = hcv(Nlab, alpha1);
Hcrit99 = hcv(Nlab, alpha2);

H95out = Hcrit95;
H99out = Hcrit99;

a=0;
for i=1:Nlab
    a=a+(m(i)-grandMean)^2;
end

h = zeros(1,Nlab);
for i=1:Nlab
    h(i)=(m(i)-grandMean)/sqrt((1/(Nlab-1))*a);
end

hPloth = figure;
plot(h,'*')
title(sprintf('Mandel''s h statistic - %s',levels{iLvl}))
xlabel('Laboratory')
ylabel('h')
xlim([1 Nlab])
set(gca,'xtick', 1:Nlab)
set(gca,'xticklabel', labs)
line(get(gca,'XLim'), - Hcrit95 * ones(1,2), 'linestyle', '--', 'color', 'blue')
line(get(gca,'XLim'), Hcrit95 * ones(1,2), 'linestyle', '--', 'color', 'blue')
line(get(gca,'XLim'), -Hcrit99 * ones(1,2), 'linestyle', '--', 'color', 'red')
line(get(gca,'XLim'), Hcrit99 * ones(1,2), 'linestyle', '--', 'color', 'red')
grid on

% Performing test and logging results
fprintf('Mandel''s h statistic - %s\n\n',levels{iLvl})
log={'Mandel''s h statistic'};
for i=1:Nlab
    if abs(h(i)) <= Hcrit95
        fprintf('h for laboratory %s is correct\n\n',labs{i})
        log = [log; sprintf('h for laboratory %s is correct',labs{i})];
    else
        if abs(h(i)) <= Hcrit99
            fprintf('h for laboratory %s shows dispersed values\n\n',labs{i})
            log = [log; sprintf('h for laboratory %s shows dispersed values',labs{i})];
        else
            fprintf('h for laboratory %s shows anomalous values\n\n',labs{i})
            log = [log; sprintf('h for laboratory %s shows anomalous values',labs{i})];
        end
    end
end
xlswrite(logFile, log, sprintf('Sheet%d',iLvl), 'A7')

% Export on file
if ~isempty(varargin)
    cd(varargin{1})
    print('-dpng', sprintf('Mandel h - %s.png', levels{iLvl}))
    cd ..
end

end