function [anovatable] = SCanova(valuesCell, lengthLab, Nlab, iLvl, levels, labs, resultFile)
% SCANOVA: Effettua il test ANOVA
%   [anovatable] = SCANOVA(values, lengthLab, Nlab, iLvl, levels, labs, resultFile)
%
%   anovatable:     results of ANOVA test
%   values:         measurements for the considered level (cell array)
%   lengthLab:      measurements per laboratory
%   numlab:         number of involved laboratories with valid measurements
%   iLvl:           current level index
%   levels:         level names
%   labs:           laboratory names
%% Preparing data
maxLen = max(lengthLab);
values = zeros(maxLen, numel(valuesCell));
for i = 1:numel(valuesCell)
    values(:,i) = [valuesCell{i}; NaN(maxLen - length(valuesCell{i}))];
end

%% ANOVA Test
[~,anovatable]=anova1(values, labs, 'off');
fprintf('Test ANOVA - %s\n',levels{iLvl})
disp(anovatable)

%% Getting variance components
% Intralaboratory variance
varr=cell2mat(anovatable(3,4));

% Interlaboratory variance
n=0;
for i=1:Nlab
    n=n+lengthLab(i)^2;
end
nj=(sum(lengthLab)-(n/sum(lengthLab)))/(Nlab-1);
vdj=cell2mat(anovatable(2,4));
varL=(vdj-varr)/nj;

% Logging variances
vartot=varr+varL;
disp('Total variance')
fprintf('%8.7f\n\n',vartot)
disp('Intralaboratory variance')
fprintf('%8.7f\n\n',varr)
varr_=(varr*100)/vartot;
fprintf('Percentage: %4.2f%%\n\n',varr_)
disp('Interlaboratory variance')
fprintf('%8.7f\n\n',varL)
varL_=(varL*100)/vartot;
fprintf('Percentage: %4.2f%%\n\n',varL_)

% Logging on spreadsheet
xlswrite(resultFile, {'ANOVA Test'}, sprintf('Sheet%d',iLvl), 'N3')
xlswrite(resultFile, anovatable,sprintf('Sheet%d',iLvl),'N4')
xlswrite(resultFile, {'Total variance'; vartot;
    'Intralaboratory variance'; varr; sprintf('Percentage: %4.2f%%',varr_);
    'Interlaboratory variance'; varL; sprintf('Percentage: %4.2f%%',varL_)}, ...
    sprintf('Sheet%d',iLvl),'N8')
end