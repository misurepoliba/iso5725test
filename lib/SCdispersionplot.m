function [hFig] = SCdispersionplot(values, Nlab, level, labs, varargin)
% SCDISPERSIONPLOT Prepares data for dispersion plot
%   SCDISPERSIONPLOT(values, Nlab, iLvl, levels, laboratories)
%
%   values: measurement cell array for current level
%   Nlab:   total number of laboratories
%   level:  level name
%   labs:   lab names (cell array)
%   varargin: if a directory is given, a .png output is produced

average = mean(cell2mat(values));
Nvalues = numel(values);

hFig = figure;

hold on
for i = 1:Nvalues
    plot(i, values{i}', 'b*')
end

title(sprintf('Dispersion plot - %s', level))
xlim([1 Nlab])
xlabel('Laboratory')
ylabel('Measurements')
set(gca, 'xtick', 1:Nlab)
set(gca, 'xticklabel', labs)

line(get(gca,'XLim'), average*ones(1,2), 'linestyle', '--', 'color', 'red')
hold off
grid on

% Export on file
if ~isempty(varargin)
    cd(varargin{1})
    print('-dpng', sprintf('Dispersion - %s.png', level))
    cd ..
end

end