function [hBoxplot] = SCboxplot(values, level, labs, varargin)
% SCBOXPLOT Produces a boxplot for each laboratory
%   SCBOXPLOT(values, lengthLab, level, labs)
%
%   values: measurement cell array for current level
%   level:  level name
%   labs:	lab names (cell array)
%   varargin: if a directory is given, a .png output is produced
lengthLab = cellfun(@numel, values);
allValues = cell2mat(values);

jj = 1;
boxGroups = zeros(size(allValues));
for i = 1:length(lengthLab)
    boxGroups(jj:jj+lengthLab(i)-1) = i*ones(length(lengthLab(i)), 1);
    jj = jj + lengthLab(i);
end

hBoxplot = figure;
boxplot(allValues, boxGroups, 'labels', labs)
title(sprintf('Boxplot - %s', level))
xlabel('Laboratory')
ylabel('Measurements')
grid on

% Export on file
if ~isempty(varargin)
    cd(varargin{1})
    print('-dpng', sprintf('Boxplot - %s.png', level))
    cd ..
end

end