function SCreprip(m, mm, s, lengthLab, Nlab, iLvl, levels, resultFile)
% SCREPRIP repeatability and reproducibility standard deviations
%   SCREPRIP(m, mm, s, lengthlvl,numlab,livello,idlvl)
%
%   m:          laboratory (cell) averages
%   mm:         grand average
%   s:          laboratory (cell) standard deviations
%   lengthLab:  valid measurements per laboratory
%   Nlab:       number of involved laboratories
%   iLvl:       current level index
%   levels:     level names
%   resultFile: result spreadsheet file path

% Repeatability variance and standard deviation
vrn = 0;
vrd = 0;
for i=1:Nlab
    vrn=vrn+(lengthLab(i)-1)*(s(i)^2);
    vrd=vrd+(lengthLab(i)-1);
end
varr=vrn/vrd;
sr=sqrt(varr);

% Interlaboratory variance
vdj = 0;
n = 0;
for i=1:Nlab
    vdj=vdj+lengthLab(i)*(m(i)-mm)^2;
    n=n+lengthLab(i)^2;
end

vdj = vdj/(Nlab-1);
nj = (sum(lengthLab)-(n/sum(lengthLab)))/(Nlab-1);
varL = (vdj-varr)/nj;

% Repeatability and reproducibility variances
varR = varr + varL;
sR = sqrt(varR);

% Console and spreadsheet logging
fprintf('Repeatability standard deviation - %s\n',levels{iLvl})
disp(sr)
fprintf('Reproducibility standard deviation - %s\n',levels{iLvl})
disp(sR)

% Logging on spreadsheet
repripExcel = {'Repeatability standard deviation'; sr; 'Reproducibility standard deviation'; sR};
xlswrite(resultFile, repripExcel, sprintf('Sheet%d', iLvl), 'K3')
end