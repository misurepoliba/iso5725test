function SCgrubbs(alpha, m, Nlab, iLvl, levels, resultFile, logFile)
% SCGRUBBS Performs Grubbs test for outliers
%   SCGRUBBS(m, Nlab, iLvl, levels, resultFile, logFile)
%
%   m:          laboratory (cell) means
%   Nlab:       number of laboratories involved
%   iLvl:       current level index
%   levels:     level names
%   resultFile: result spreadsheet file
%   logFile:    log spreadsheet file

% FIXME translate comments
% Verifica se la media di qualche cella risulti eccessivamente alta o
% bassa, tale da aumentare sensibilmente lo scarto tipo di riproducibilitÓ
% se non eliminata

% Matrici contenenti i valori critici per il test di Grubbs
% Ad ogni riga corrisponde un numero di laboratori partecipanti (valori
% per numlab 3:20)

% Checking significance levels
alpha1 = alpha(1); alpha2 = alpha(2);
if alpha1 <= alpha2
    error('Incorrect significance levels')
end

% Analytical functions for critical values
gsc = @(p, alpha) (p-1) .* tinv(1 - alpha./p, p - 2) ./ ...
    sqrt(p.*(p - 2 + (tinv(1 - alpha./p, p - 2)).^2));

alpha_ = [0.0010, 0.0050, 0.0100, 0.0250, 0.0500, 0.1000];
a_ = [0.0443, 0.0388, 0.0362, 0.0322, 0.0289, 0.0251];
b_ = [1.0012, 0.9558, 0.9250, 0.8833, 0.8501, 0.8169];
c_ = [-4.2493, -3.6613, -3.3101, -2.8580, 2.5075, 2.1615];

fgrubbs = @(p, alpha) interp1(alpha_, a_, alpha)*p.^2 + ...
    interp1(alpha_, b_, alpha)*p + interp1(alpha_, c_, alpha);
gdc = @(p, alpha) 1 ./ (1 + 2./(p - 3).*finv((1-alpha).^(1./fgrubbs(p, alpha)), 2, p - 3));

%% Single outlier test
% High
Gsa=(max(m)-mean(m))/std(m);

% Low
Gsb=(mean(m)-min(m))/std(m);

% Critical values
Gscrit95 = gsc(Nlab, alpha1);
Gscrit99 = gsc(Nlab, alpha2);

% Console logging
fprintf('Grubbs test - %s\n', levels{iLvl})
disp('Single (high)')
disp(Gsa)
disp('Single (low)')
disp(Gsb)
disp('95% critical value')
disp(Gscrit95)
disp('99% critical value')
disp(Gscrit99)

%% Logging test on single outlier Grubbs test
log={'Grubbs test'};
if Gsa <= Gscrit95
    fprintf('Grubbs test for single high outlier (%s) does not show anomalies\n\n',levels{iLvl})
    log=[log;{'Grubbs test for single high outlier does not show anomalies'}];
else
    if Gsa<=Gscrit99
        fprintf('Grubbs test for single high outlier (%s) shows dispersed values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for single high outlier shows dispersed values'}];
    else
        fprintf('Grubbs test for single high outlier (%s) shows anomalous values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for single high outlier shows dispersed values'}];
    end
end

if Gsb<=Gscrit95
    fprintf('Grubbs test for single low outlier (%s) does not show anomalies\n\n',levels{iLvl})
    log=[log;{'Grubbs test for single low outlier does not show anomalies'}];
else
    if Gsb <= Gscrit99
        fprintf('Grubbs test for single low outlier (%s) shows dispersed values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for single low outlier shows dispersed values'}];
    else
        fprintf('Grubbs test for single low outlier (%s) shows anomalous values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for single low outlier shows anomalous values'}];
    end
end

%% Checking need to test double outliers
if Gsa > Gscrit99 || Gsb > Gscrit99
    grubbsexcel={'Grubbs test'; 'Single high'; Gsa; 'Single low'; Gsb;
        '95% critical value'; Gscrit95; '99% critical value'; Gscrit99};
    xlswrite(resultFile, grubbsexcel, sprintf('Sheet%d',iLvl), 'I3')
    xlswrite(logFile, log, sprintf('Sheet%d',iLvl), sprintf('A%d',(2*Nlab+14)))
    return
end

%% Double outlier test

% Double high
mg=sort(m);
x=0;
for i=1:length(mg)
    x=x+(mg(i)-mean(mg))^2;
end
mda=mg(1:(length(mg)-2));
y=0;
for i=1:length(mda)
    y=y+(mda(i)-mean(mda))^2;
end
Gda=y/x;

% Double low
mdb=mg(3:length(mg));
z=0;
for i=1:length(mdb)
    z=z+(mdb(i)-mean(mdb))^2;
end
Gdb=z/x;

% Critical values
Gdcrit95 = gdc(Nlab, alpha1);
Gdcrit99 = gdc(Nlab, alpha2);
% Gdcrit95=Gd95((Nlab-2));
% Gdcrit99=Gd99((Nlab-2));

% Logging double test results
disp('Double high')
disp(Gda)
disp('Double low')
disp(Gdb)
disp('95% critical value')
disp(Gdcrit95)
disp('99% critical value')
disp(Gdcrit99)

%% Logging on results spreadsheet
grubbsexcel={'Grubbs test'; 'Single high'; Gsa; 'Single low'; Gsb;
    '95% critical value'; Gscrit95; '99% critical value'; Gscrit99;
    'Double high'; Gda; 'Double low'; Gdb; '95% critical value';
    Gdcrit95; '99% critical value'; Gdcrit99};
xlswrite(resultFile, grubbsexcel, sprintf('Sheet%d',iLvl),'I3')

%% Logging double test FIXME reorder logging sections
if Gda>=Gdcrit95
    fprintf('Grubbs test for double high outliers (%s) does not show anomalies\n\n',levels{iLvl})
    log=[log;{'Grubbs test for double high outliers does not show anomalies'}];
else
    if Gda>=Gdcrit99
        fprintf('Grubbs test for double high outliers (%s) shows dispersed values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for double high outliers shows dispersed values'}];
    else
        fprintf('Grubbs test for double high outliers (%s) shows anomalous values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for double high outliers shows anomalous values'}];
    end
end

if Gdb>=Gdcrit95
    fprintf('Grubbs test for double low outliers (%s) does not show anomalies\n\n',levels{iLvl})
    log=[log;{'Grubbs test for double low outliers does not show anomalies'}];
else
    if Gdb>=Gdcrit99
        fprintf('Grubbs test for double low outliers (%s) shows dispersed values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for double low outliers shows dispersed values'}];
    else
        fprintf('Grubbs test for double low outliers (%s) shows anomalous values\n\n',levels{iLvl})
        log=[log;{'Grubbs test for double low outliers shows anomalous values'}];
    end
end

xlswrite(logFile, log, sprintf('Sheet%d',iLvl), sprintf('A%d',(2*Nlab+14)))
end