function SCcochran(alpha, var, lengthLab, Nlab, iLvl, levels, resultFile, logFile)
%% SCCOCHRAN Effettua il test di Cochran FIXME translate docs
%   SCCOCHRAN(var,lengthlvl,numlab,livello,idlvl)
%
%   var:        laboratory (cell) variance
%   lengthLab:  valid measurements per laboratory
%   Nlab:       number of involved laboratories
%   livello:    livello corrente della prova
%   idlvl :     Identificativo del livelli

% Verifica se lo scarto tipo di qualche cella sia eccessivamente elevato e
% tale da aumentare sensibilmente lo scarto tipo di ripetibilit� se non
% eliminato

alpha1 = alpha(1); alpha2 = alpha(2);
if alpha1 <= alpha2
    error('Incorrect significance levels')
end

C99n2=[nan; 0.993; 0.968; 0.928; 0.883; 0.838; 0.794; 0.754; 0.718; 0.684; 0.653; 0.624; 0.599; 0.575; 0.553; 0.532; 0.514; 0.496; 0.480];
C95n2=[nan; 0.967; 0.906; 0.841; 0.781; 0.727; 0.680; 0.638; 0.602; 0.570; 0.541; 0.515; 0.492; 0.471; 0.452; 0.434; 0.418; 0.403; 0.389];
C99n3=[0.995; 0.942; 0.864; 0.788; 0.722; 0.664; 0.615; 0.573; 0.536; 0.504; 0.475; 0.450; 0.427; 0.407; 0.388; 0.372; 0.356; 0.343; 0.330];
C95n3=[0.975; 0.871; 0.768; 0.684; 0.616; 0.561; 0.516; 0.478; 0.445; 0.417; 0.392; 0.371; 0.352; 0.335; 0.319; 0.305; 0.293; 0.281; 0.270];
C99n4=[0.979; 0.883; 0.781; 0.696; 0.626; 0.568; 0.521; 0.481; 0.447; 0.418; 0.392; 0.369; 0.349; 0.332; 0.316; 0.301; 0.288; 0.276; 0.265];
C95n4=[0.939; 0.798; 0.684; 0.598; 0.532; 0.480; 0.438; 0.403; 0.373; 0.348; 0.326; 0.307; 0.291; 0.276; 0.262; 0.250; 0.240; 0.230; 0.220];
C99n5=[0.959; 0.834; 0.721; 0.633; 0.564; 0.508; 0.463; 0.425; 0.393; 0.366; 0.343; 0.322; 0.304; 0.288; 0.274; 0.261; 0.249; 0.238; 0.229];
C95n5=[0.906; 0.746; 0.629; 0.544; 0.480; 0.431; 0.391; 0.358; 0.331; 0.308; 0.288; 0.271; 0.255; 0.242; 0.230; 0.219; 0.209; 0.200; 0.192];
C99n6=[0.937; 0.793; 0.676; 0.588; 0.520; 0.466; 0.423; 0.387; 0.357; 0.332; 0.310; 0.291; 0.274; 0.259; 0.246; 0.234; 0.223; 0.214; 0.205];
C95n6=[0.877; 0.707; 0.590; 0.506; 0.445; 0.397; 0.360; 0.329; 0.303; 0.281; 0.262; 0.243; 0.232; 0.220; 0.208; 0.198; 0.189; 0.181; 0.174];
% Matrici contenenti i valori critici per il test di Cochran
% Ad ogni riga corrisponde un numero di laboratori partecipanti (valori
% per numlab 2:20) FIXME translate
C95=[C95n2 C95n3 C95n4 C95n5 C95n6];
C99=[C99n2 C99n3 C99n4 C99n5 C99n6];

% NB Il criterio di Cochran si pu� applicare solo nel caso in cui gli
% scarti tipo siano stati tutti calcolati con il medesimo numero di
% risultati di prova n. Tuttavia nel caso in cui il numero n non sia uguale
% per tutti i laboratori e le sue variazioni siano limitate si pu� assumere
% come valore di n quello che figura nella maggior parte delle celle FIXME
% translate
C=max(var)/sum(var);
n=round(mean(lengthLab));

cc = @(p, n, alpha) 1 ./ (1 + (p-1).*finv(alpha./p, (p-1)*(n-1), n-1));

Ccrit95 = cc(Nlab, n, alpha1);
Ccrit99 = cc(Nlab, n, alpha2);
% Ccrit95=C95((Nlab-1),(n-1));
% Ccrit99=C99((Nlab-1),(n-1));

fprintf('Cochran test - %s\n',levels{iLvl})
disp(C)
disp('95% critical value')
disp(Ccrit95)
disp('99% critical value')
disp(Ccrit99)
    
%% Logging test results
cochranexcel={'Cochran test'; C; '95% critical values'; Ccrit95; ...
    '99% critical value'; Ccrit99};

xlswrite(resultFile, cochranexcel, sprintf('Sheet%d', iLvl), 'G3')

log={'Cochran test'};
if C<=Ccrit95
    fprintf('Cochran test for %s does not show anomalies\n\n',levels{iLvl})
    log=[log;{'Cochran test does not show anomalies'}];
else
    if C<=Ccrit99
        fprintf('Cochran test for %s shows dispersed values\n\n',levels{iLvl})
        log=[log;{'Cochran test shows dispersed values'}];
    else
        fprintf('Cochran test for %s shows anomalies\n\n',levels{iLvl})
        log=[log;{'Cochran test for %s shows anomalies'}];
    end
end
xlswrite(logFile, log, sprintf('Sheet%d', iLvl), sprintf('A%d',(2*Nlab+11)))
end